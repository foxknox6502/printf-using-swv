# Printf via SWO/SWV


## Description

This project demonstrates how the Serial Wire Output (SWO) can be used for `printf()` re-direction to display data in a SWV Debug Console without any USB to serial converter in **Atollic TrueSTUDIO for STM32**.



> Care must be taken when using `HAL_Delay()`, this function provides accurate delay (in milliseconds) based on variable incremented in _SysTick_ ISR. This implies that if `HAL_Delay()` is called from a peripheral ISR process, then the _SysTick_ interrupt must have higher priority (numerically lower) than the peripheral interrupt. Otherwise the caller ISR process will be blocked. To change the _SysTick_ interrupt priority you have to use `HAL_NVIC_SetPriority()` function.

> The application needs to ensure that the _SysTick_ time base is always set to 1 millisecond to have correct HAL operation.


## Making printf() to output data via SWO

Serial Wire Output (SWO) is single-pin, asynchronous serial communication channel available on Cortex-M3/M4/M7, it is using the ITM (Instrumentation Trace Macrocell) module of the Cortex Core-Sight.

In order to ensure SWO pin (PB3) is not allocated to another use, in STM32CubeMX, select "Trace Asynchronous Sw" in "SYS", no specific code will be generated.

![alt](https://i.imgur.com/r07CmfH.png)

To re-direct `printf()` output, you need to change how `printf()` output its data. On the GNU GCC compiler using the Newlib or Newlib Nano libraries, `printf()` calls the function `_write()` to output the data to wherever it is supposed to be sent.

In **Atollic TrueSTUDIO for STM32**, select File > New > Other, and create a new `syscalls.c` file.

![alt](https://i.imgur.com/IfwmgFN.png)

To make `printf()` to output data to the SWV Debug Console, change the `_write()` function implementation in `syscalls.c` file.

```c
int _write(int32_t file, uint8_t *ptr, int32_t len)
{
	/* Implement your write code here, this is used by puts and printf for example */
	for (int32_t i = 0; i < len; ++i)
	{
		ITM_SendChar((uint32_t)(*ptr++));
	}
	return len;
	
	/*errno = ENOSYS;*/
	/*return -1;*/
}
```

Do not forget to include the `stm32f4xx.h` file, which contains the `ITM_SendChar()` implementation.


## Configuring the SWO Console to output data

To be able to watch the ITM output in the debugger console. Select Run > Run Configurations:
- Select **ST-LINK** as debug probe.
- Enable **Serial Wire Viewer (SWV)** and enter the correct Core Clock frequency.
- Click **Debug** to start a new debugging session.

![alt](https://i.imgur.com/janQ9eW.png)

Open the SWV settings panel by clicking on the Configure Serial Wire Viewer button in the SWV Console view toolbar, and enable **ITM Stimulus Port 0**.

![alt](https://i.imgur.com/sm808ND.png)

Press the red Start/Stop Trace button to send the SWV configuration to the target board and enable SWV trace recording. Then start the target execution.

![alt](https://i.imgur.com/NoS1AN6.png)


## Directory contents

```
.
├── Core
│   ├── Inc
│   │   ├── main.h                  Main program header file
│   │   ├── stm32f4xx_hal_conf.h    HAL Configuration file
│   │   └── stm32f4xx_it.h          Interrupt handlers header file
│   └── Src
│       ├── main.c                  Main program
│       ├── stm32f4xx_hal_msp.c     HAL MSP module
│       ├── stm32f4xx_it.c          Interrupt handlers
│       ├── syscalls.c              Minimal system calls implementation
│       └── system_stm32f4xx.c      STM32F4xx system clock configuration file
├── project.elf.launch
├── project.ioc                     STM32CubeMX project settings
├── README.md
├── startup
│   └── startup_stm32f407xx.s       STM32F407xx Devices vector table for GCC based toolchains
└── STM32F407VG_FLASH.ld            Linker script for STM32F407VGTx Device
```

## Hardware and Software environment

- This workspace is provided for **Atollic TrueSTUDIO for STM32**.
- This example runs on STM32F407xx devices.
- This example has been tested with STMicroelectronics STM32F4-Discovery RevB & RevC boards and can be easily tailored to any other supported device and development board.


## How to use it ?

In order to make the program work, you must do the following:
- Import this project into **Atollic TrueSTUDIO for STM32**.
- Rebuild all files and load your image into target memory.
- Run the example.


## Useful documents

- [Debug Access - CMSIS-Core (Cortex-M)](https://www.keil.com/pack/doc/CMSIS/Core/html/group__ITM__Debug__gr.html)
- [AN4989](https://www.st.com/content/ccc/resource/technical/document/application_note/group0/3d/a5/0e/30/76/51/45/58/DM00354244/files/DM00354244.pdf/jcr:content/translations/en.DM00354244.pdf): STM32 microcontroller debug toolbox
- [RM0090](https://www.st.com/content/ccc/resource/technical/document/reference_manual/3d/6d/5a/66/b4/99/40/d4/DM00031020.pdf/files/DM00031020.pdf/jcr:content/translations/en.DM00031020.pdf): STM32F4xx Reference Manual
    - See `38.16.3` section: Debug MCU configuration register


<small><center>Copyright (C) 2018 Ángel G.</center></small>